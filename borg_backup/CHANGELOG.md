# Changelog

## 0.3.7

- Fix api path

## 0.3.6

- Fix api path

## 0.3.5

- Get backup file from name

## 0.3.4

- Fix unzip path

## 0.3.3

- Fix logic

## 0.3.2

- Fix syntax

## 0.3.1

- Add tar dependency

## 0.3.0

- Skip repo initialization, make sure your repo is already initialized
- Add unzip option

## 0.2.0

- Add file parameter to only backup a single archive
- Fix translations

## 0.1.5

- Fix file permissions

## 0.1.4

- Fix breaking change

## 0.1.3 

- Fix error despite running successfully

## 0.1.2

- Bugfixes
- Dont ask for ssh key pass

## 0.1.1

- Make ssh-keygen quiet

## 0.1.0

- Initial Release

