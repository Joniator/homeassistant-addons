#! /bin/bash
# shellcheck shell=bash

set -e # abort on errors
set -u # abort on unset variables

TRAEFIK_CERT_STORE="{{ .traefik.store }}"
TRAEFIK_RESOLVER="{{ .traefik.resolver }}"
DOMAIN="{{ .traefik.domain }}"

# minor sanity checks
if [ ! -r "$TRAEFIK_CERT_STORE" ]; then
    echo "File $TRAEFIK_CERT_STORE not readable!"
    exit 1
fi
if ! grep "\"${DOMAIN}\"" "$TRAEFIK_CERT_STORE" > /dev/null; then
    echo "Domain $DOMAIN not found in $TRAEFIK_CERT_STORE"
    exit 1
fi

# create new files with strict permissions (mktemp defaults to 600)
KEY_FILE="$(mktemp --tmpdir XXXXX.key)"
trap 'rm -f "$CERT_FILE"' EXIT
CERT_FILE="$(mktemp --tmpdir XXXXX.crt)"
trap 'rm "$KEY_FILE"' EXIT

# extract certificate
jq -r ".${TRAEFIK_RESOLVER}.Certificates[] | select(.domain.main==\"${DOMAIN}\") | .certificate" < "$TRAEFIK_CERT_STORE" | base64 -d > "$CERT_FILE"

# extract private key
jq -r ".${TRAEFIK_RESOLVER}.Certificates[] | select(.domain.main==\"${DOMAIN}\") | .key" < "$TRAEFIK_CERT_STORE" | base64 -d > "$KEY_FILE"

tar -cf - "$CERT_FILE" "$KEY_FILE" 2> /dev/null | base64