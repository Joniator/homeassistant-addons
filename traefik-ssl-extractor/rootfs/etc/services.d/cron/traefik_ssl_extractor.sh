#!/usr/bin/env bashio
# shellcheck shell=bash

set -e # abort on errors
set -u # abort on unset variables

HOST="$(bashio::config 'host')"
DOMAIN="$(bashio::config 'traefik.domain')"

bashio::log.info "Extracting cert and key from $HOST"
RESPONSE=$(ssh "$HOST" -i ~/.ssh/id 'sudo bash -s' < /etc/services.d/cron/cert-downloader.sh)

echo "$RESPONSE" | base64 -d | tar -xC /tmp 2> /dev/null

NEW_CERT_FILE="$(ls /tmp/tmp/*.crt)"
NEW_KEY_FILE="$(ls /tmp/tmp/*.key)"
chmod 640 "$NEW_CERT_FILE" "$NEW_KEY_FILE"
trap 'rm -f "$NEW_CERT_FILE" "$NEW_KEY_FILE"' EXIT

if [[ ! -d /ssl ]]; then
    bashio::log.warning "Directory /ssl does not exist, creating."
    mkdir /ssl
fi
CERT_FILE="/ssl/$DOMAIN.crt"
KEY_FILE="/ssl/$DOMAIN.key"

# check if the contents changed
if ! diff -N "$NEW_CERT_FILE" "$CERT_FILE" > /dev/null; then
    mv "$NEW_CERT_FILE" "$CERT_FILE"
    mv "$NEW_KEY_FILE" "$KEY_FILE"
    bashio::log.info "Certificate for $DOMAIN updated"
else
    # certificate unchanged, delete temporary files
    bashio::log.info "Certificate $DOMAIN unchanged"
fi
exit
