# Changelog

## 1.0.9

- Fix breaking change

## 1.0.8

- Add version tag

## 1.0.7

- Run script with sudo to read acme.json
- Show date with logging

## 1.0.6

- Revert changes from 1.0.5
- Change logging

## 1.0.5

- Accept multiline string as private key instead of array
## 1.0.4

- Include date in log outputs

## 1.0.3

- Ensure temporary cert files are removed
- Check, warn and create if /ssl-Folder does not exists

## 1.0.2

- Disable tracing
- ShellCheck refactorings

## 1.0.1

- Enable tracing
- Enforce scripts as executable

## 1.0.0

- Initial Release

